# Hardened Alpine Linux on a Laptop
----------------------------------

Alpine Linux is a minimal and security focused Linux distribution geared towards power users. Alpine is developed for users who want a minimal security focused system that is lightweight and resource efficient. This guide is to help you install and configure a hardened Alpine Linux system for general use. In this guide we will setup Alpine Linux using:

* Full Disk Encryption (to keep our data and system secure)

* LVM (to help break things up into multiple logical partitions to keep things flexible and dynamic; this also helps add some extra security)

## Getting Started

The first step is to gather everything we will need for our install. We first need to download the Alpine Standard iso image, to do this go to [Alpine Linux download page](https://www.alpinelinux.org/downloads/) and select the standard iso; or simply run:

	wget http://dl-cdn.alpinelinux.org/alpine/v3.9/releases/x86_64/alpine-standard-3.9.4-x86_64.iso

After you have finished downloading the iso image, you'll need to create a bootable media with it. Grab a USB flash drive and plug it into your computer, then open a terminal and follow these steps to create a bootable USB using cdrecord (these steps assume your flash drive is mounted as /dev/sdb, to find your flash drive use fdisk -l; for further help search [Duckduckgo](https://duckduckgo.com/)):

	cdrecord -v dev=/dev/sdb speed=4 alpine-standard-3.8.0-x86.iso

Plug the USB into your laptop and boot off of it from your BiOS boot menu, then at the prompt login as root with no password (IE, username: root password: empty).

## Preparing the Temporary Enviroment

Just a side note, **everything setup in this enviroment will not carry over to the installed system**. With that said lets get started! We are going to be setting up temporary installation enviroment, this will setup our keymapping, network interface, repositories and everything we will need to do the installation.

First thing to do is setup your keyboard language. The default is US QWERTY so if that is what you use you can skip this part. To configure your keyboard language run:

	setup-keymap

and pick your keyboard language. Next, configure your network interface. Run:

	setup-interfaces

and follow the instructions. Additionally, if you set a static IP address you'll need to configure DNS to be able to resolve host names; to do this run:

	setup-dns

Now we'll need to enable our network interface to do so we'll use `ifup`, for example:

	ifup wlan0

Next we need to set an apk repository and update our cache. To do this run the following:

	setup-apkrepos
	apk update

Lastly, we'll need to install all the packages required to set up LVM and LUKS. Install the needed packages using the following command:

	apk add haveged lvm2 cryptsetup e2fsprogs syslinux

Now start the haveged service to be used to get random numbers for encryption:

	rc-service haveged start

## Preparing the Disk

The very first thing we need to do to our drive is wipe it and the dead space on it. **This will delete all data on the drive!** To do this run the following (this assumes your hard drive is /dev/sda), as a side note; if your last install was encrypted then you don't need to do this as the keys will be lost anyway:

	dd if=/dev/urandom of=/dev/sda bs=1M

Next we will create the partitions using the `fdisk` utility.

	fdisk /dev/sda

Create the `/boot/` partiton:

	n
	p
	1
	1
	+100m

This will create a new 100 MB primary partition. Now we will set the `/boot/` partition active:

	a
	1

Now we will create the LVM partition:

	n
	p
	2
	press **enter** to select the default start cylinder
	press **enter** to select the maximum available size

Then set the partition type for the LVM PV:

	t
	2
	8e

Now lets verify the settings of our drive:

	p

This should show two partitions, `/dev/sda1` which should be set to boot and `/dev/sda2` which should be the rest of the drive.

Save our changes to the drive:

	w

Wipe the LVM PV partition with random values (note that depending on the size of the partition this can take awhile, anywhere from a few minuets to a few hours):

	haveged -n 0 | dd of=/dev/sda2

## Encrypt the LVM Physical Volume Partition and Create the Logical Volumes and File Systems

Encrypt the partition which will later contain the LVM PV:

	cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/sda2

Open the LUKS partition:

	cryptsetup open --type luks /dev/sda2 crypto

Create the PV on /dev/sda:

	pvcreate /dev/mapper/crypto

Create the vg0 LVM VG in the /dev/mapper/crypto PV:

	vgcreate vg0 /dev/mapper/crypto

Now we will create the logical volumes.

Create a 70 GB logical volume named root in the vg0 VG:

	lvcreate -L 70G vg0 -n root

Create a 4 GB swap logical volume:

	lvcreate -L 4G vg0 -n swap

Create a 100%FREE home logical volume:

	lvcreate -n home -l 100%FREE vg0

The logical volumes created are automatically marked active. Verify this by running:

	lvscan

Format the root logical volume using the ext4 file system:

	mkfs.ext4 /dev/vg0/root

Format the home logical volume using the ext4 file system:

	mkfs.ext4 /dev/vg0/home

Format the swap logical volume:

	mkswap /dev/vg0/swap

Format the `/dev/sda1` device for the `/boot/` partition using the ext2 file system:

	mkfs.ext2 /dev/sda1

## Mount the filesystems:

Before we install Alpine Linux we'll have to mount our partitions and logical volumes.

Mount the root logical volume to the `/mnt/` directory:

	mount -t ext4 /dev/vg0/root /mnt/

Create `/mnt/boot/` directory and mount the `/dev/sda1` partition in this directory:

	mkdir /mnt/boot/
	mount -t ext2 /dev/sda1 /mnt/boot/

## Installing Alpine Linux

Now we are ready to install Alpine Linux!

Install Alpine Linux:

	setup-disk -m sys /mnt/

The installer downloads the latest packages to install the base installation. The installer will also automatically create the entries for the mount points in the `fstab` file, which are currently mounted in the `/mnt/` directory. The automatic writing of the master boot record (MBR) will fail in this step, however don't worry; You will manually write the MBR to the disk later.

To enable the operating system to decrypt the PV at boot time, create the `/mnt/etc/crypttab` file. Enter the following line into the file to decrypt the `/dev/sda2` partition using the luks module and map it to the crypto name:

	vi /mnt/etc/crypttab:

		crypto    /dev/sda2    none    luks

The swap logical volume is not automatically added to the `fstab` file, thus we need to add it ourselves.

	vi /mnt/etc/fstab:

		/dev/vg0/swap    swap    swap    defaults    0 0

Edit the `/mnt/etc/mkinitfs/mkinitfs.conf` file and append the cryptsetup module to the features parameter:

	vi /mnt/etc/mkinitfs/mkinitfs.conf:

		features="ata base ide scsi usb virtio ext4 lvm cryptsetup"

Now we need to rebuild the initial RAM disk:

	mkinitfs -c /mnt/etc/mkinitfs/mkinitfs.conf -b /mnt/ $(ls /mnt/lib/modules/)

Edit the `/mnt/etc/update-extlinux.conf` file and append the following kernel options to the `default_kernel_opts` parameter:

	vi /mnt/etc/update-extlinux.conf:
		default_kernel_opts="... cryptroot=/dev/sda2 cryptdm=crypto"

Because the update-extlinux utility operators only on the `/boot/` directory, temporarily change the root to the `/mnt/` directory and update the boot loader configuration:

	chroot /mnt/
	update-extlinux
	exit

Ignore the errors the update-extlinux utility displays.

Write the MBR to the `/dev/sda` device:

	dd bs=440 count=1 conv=notrunc if=/mnt/usr/share/syslinux/mbr.bin of=/dev/sda

## Unmounting the Volumes and Partitions

Umount `/mnt/boot/` and `/mnt/`:

	umount /mnt/boot/
	umount /mnt/

Disable the swap partition:

	swapoff -a

Deactivate the VG:

	vgchange -a n

Close the crypto device:

	cryptsetup luksClose crypto

Reboot the system:

	reboot
